#Webhook parser

This project uses:

    1. Java 8
    2. Maven 3.3 or above
    3. Spring Boot

To build the project use the command:

    mvn clean package

To run the project from source using the default log file provided with this project:

    mvn spring-boot:run

Or, you can specify a log file:

    mvn spring-boot:run -Drun.arguments={file-name}

And where are the unit tests?

    Timed out and only 1 unit test. WHAT A SHAME! :-(
    I know... and I won't sleep tonight because that.
