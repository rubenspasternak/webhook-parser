package br.com.moip.parser;

import java.nio.file.NoSuchFileException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class WebhookParserTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void printReport() throws Exception {
        // TODO
    }

    @Test
    public void inputFileNotFound() throws Exception {
        expectedException.expect(NoSuchFileException.class);
        WebhookParser parser = new WebhookParser("test1.log");
    }

}