package br.com.moip;

import java.text.ParseException;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.moip.parser.WebhookParser;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Override
    public void run(final String... args) throws Exception {

        String fileName;
        if (args.length == 1) {
            // File name received from command line
            fileName = args[0];
        } else {
            // Default file name
            fileName = "./logs/log.txt";
            System.out.println("Assuming default file: " + fileName);
        }

        WebhookParser parser = new WebhookParser(fileName);
        parser.printReport();
    }

    public static void main(String args[]) throws ParseException {
        SpringApplication.run(Application.class, args);
    }

}
