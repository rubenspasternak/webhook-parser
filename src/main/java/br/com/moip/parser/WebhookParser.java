package br.com.moip.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import br.com.moip.model.Webhook;

public class WebhookParser {

    private static final Pattern LINE_PATTERN = Pattern.compile("level=(\\S+?) response_body=(\\S+?) request_to=(\\S+?) response_headers=(.+?) response_status=(\\S+?)");
    private Map<String, Integer> urlMap = new HashMap<>();
    private Map<String, Integer> codeMap = new HashMap<>();

    public WebhookParser(String fileName) throws IOException {

        Stream<String> stream = this.readFile(fileName);

        stream.forEach(line -> {
            Webhook webhook = this.parseLine(line);

            if (webhook != null) {
                this.analizeWebhook(webhook);
            }
        });
    }

    private Stream<String> readFile(String fileName) throws IOException {
        Stream<String> stream = null;

        try {
            System.out.println("Reading file: " + fileName);
            stream = Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            throw e;
        }

        return stream;
    }

    private Webhook parseLine(String line) {

        String level;
        String responseBody;
        String requestTo;
        String responseHeaders;
        String responseStatus;

        Matcher matcher = LINE_PATTERN.matcher(line);
        if (matcher.matches()) {
            level = matcher.group(1);
            responseBody = matcher.group(2);
            requestTo = matcher.group(3);
            responseHeaders = matcher.group(4);
            responseStatus = matcher.group(5);

            return new Webhook(level, responseBody, requestTo, responseHeaders, responseStatus);
        }
        return null;
    }

    private void analizeWebhook(Webhook webhook) {
        String urlValue = webhook.getRequestTo();
        int urlCount = urlMap.getOrDefault(urlValue, 0) + 1;
        urlMap.put(urlValue, urlCount);

        String statusValue = webhook.getResponseStatus();
        Integer statusCount = codeMap.getOrDefault(statusValue, 0) + 1;
        codeMap.put(statusValue, statusCount);
    }

    public void printReport() {

        System.out.println("Top 3 urls:");
        //@formatter:off
        urlMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(3)
                .forEach(entry -> System.out.println(entry.getKey() + " - " + entry.getValue()));
        //@formatter:on

        System.out.println();

        System.out.println("Webhooks by status:");
        //@formatter:off
        codeMap.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(entry -> System.out.println(entry.getKey() + " - " + entry.getValue()));
        //@formatter:on
    }

}
