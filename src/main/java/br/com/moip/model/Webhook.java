package br.com.moip.model;

public class Webhook {

    private String level;
    private String responseBody;
    private String requestTo;
    private String responseHeaders;
    private String responseStatus;

    public Webhook(String level, String responseBody, String requestTo, String responseHeaders, String responseStatus) {
        this.level = level;
        this.responseBody = responseBody;
        this.requestTo = requestTo;
        this.responseHeaders = responseHeaders;
        this.responseStatus = responseStatus;
    }

    public String getLevel() {
        return level;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public String getRequestTo() {
        return requestTo;
    }

    public String getResponseHeaders() {
        return responseHeaders;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

}
